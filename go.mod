module git.sr.ht/~ancarda/tls-redirector

go 1.13

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/spf13/afero v1.3.4
	github.com/stretchr/testify v1.6.1
)
